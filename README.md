## Spring Boot and Thymeleaf project
By Emil Grenebrant, Kristoffer Haglund and Martin Sandström
* Emil:
Responsible for git, added database connection class (database helper), end-point mappings, front-end
* Kristoffer:
Created database, queries and filling the database, end-point mappings, front-end
* Martin:
Thymeleaf pages, index.html, end-point mappings, front-end