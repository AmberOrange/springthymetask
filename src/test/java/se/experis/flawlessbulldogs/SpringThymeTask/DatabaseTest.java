package se.experis.flawlessbulldogs.SpringThymeTask;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import se.experis.flawlessbulldogs.SpringThymeTask.helpers.Database;
import se.experis.flawlessbulldogs.SpringThymeTask.models.Contact;

import java.sql.*;
/*
 * In order to test the querys that was going to be used to extract the requaried data
 * this test class was created
 * The test class is testing to create a database, insert new data,
 * serach after a contact by name, delete a row and get all the infromation in the database
 */


@SpringBootTest
class DataBaseTest {
    private static String URL = "jdbc:sqlite::resource:contactListDatabase.db";
    private static Connection conn = null;


    @Test
    public void openConnection() {
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            conn.close();
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {

        }
    }
    // Creating new data base
    @Test
    public  void createTable() {

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("CREATE TABLE IF NOT EXISTS contactList(id integer PRIMARY KEY AUTOINCREMENT, name text not null, email text not null, contactNumber text not null)");
            preparedStatement.execute();
            System.out.println("Database created");
            conn.close();
        }catch (SQLException e) {
            System.out.println(e.getMessage() + "something went wrong");
        }
    }
    // Inserting new data
    @Test
    public void insertDataIntoDatabase() {
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO contactList(name, email, contactNumber) VALUES (?, ? ,?)");

            preparedStatement.setString(1, "adaw");
            preparedStatement.setString(2, "awdad");
            preparedStatement.setString(3, "awdad");
            preparedStatement.execute();
            conn.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    // Reciveing information from contact
    @Test
    public String getInformation() {
        StringBuilder stringBuilder = null;
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT id,name,email,contactNumber from contactList where name=?");
            preparedStatement.setString(1,"awdad");
            ResultSet resultSet = preparedStatement.executeQuery();
            stringBuilder = new StringBuilder();
            while (resultSet.next()) {
                stringBuilder.append(resultSet.getInt("id"));
                stringBuilder.append(resultSet.getString("name"));
                stringBuilder.append(resultSet.getString("email"));
                stringBuilder.append(resultSet.getString("contactNumber"));
            }
        }catch (SQLException e) {
            System.out.println("Something went wrong " + e.getMessage());
        }
        return stringBuilder.toString();
    }
    // drop row
    @Test
    public void dropRow() {
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("DELETE FROM contactList WHERE name=?");
            preparedStatement.setString(1,"awdad");
            preparedStatement.execute();

        }catch (SQLException e) {
            System.out.println("Something went wrong " + e.getMessage());
        }
    }
    // getting att the information stored in database
    @Test
    public void getAll() {
        StringBuilder stringBuilder = null;
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT * from contactList");
            ResultSet resultSet = preparedStatement.executeQuery();
            stringBuilder = new StringBuilder();
            while (resultSet.next()) {
                stringBuilder.append(resultSet.getInt("id"));
                stringBuilder.append(resultSet.getString("name"));
                stringBuilder.append(resultSet.getString("email"));
                stringBuilder.append(resultSet.getString("contactNumber"));
            }

        }catch (SQLException e) {
            System.out.println("Something went wrong " + e.getMessage());
        }
        //return stringBuilder.toString();
    }

    @Test
    void useDatabaseHelperToQuery() {
        Database database = new Database("resource:contactListDatabase.db");
        ResultSet resultSet = database.getResultSetFromQuery("SELECT * from contactList");
        Contact contact;
        try {
            while(resultSet.next()) {
                contact = new Contact(
                        resultSet.getString("name"),
                        resultSet.getString("email"),
                        resultSet.getString("contactNumber"));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
