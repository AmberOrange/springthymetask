package se.experis.flawlessbulldogs.SpringThymeTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import se.experis.flawlessbulldogs.SpringThymeTask.helpers.Database;

@SpringBootApplication
public class SpringThymeTaskApplication {

	public static void main(String[] args) {
		if(Database.database == null) {		// This doesn't seem to do anything
			System.out.println("Initializing the database...");
			Database.database = new Database("resource:contactListDatabase.db");
		}
		SpringApplication.run(SpringThymeTaskApplication.class, args);
	}

}
