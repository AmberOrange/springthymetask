package se.experis.flawlessbulldogs.SpringThymeTask.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.flawlessbulldogs.SpringThymeTask.helpers.Database;
import se.experis.flawlessbulldogs.SpringThymeTask.models.Contact;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


// Using a controller and not a rest controller
@Controller
public class DatabaseController {

    // The idea behind the back-end and the mapping was to have it work like a REST-api
    // and assume that thymeleaf and HTML would work the same way.
    // As we learnt the hard way, that's not how it works as things like HTML forms
    // only supports GET and POST methods.
    // The final solution tried to make our end-point bindings work with the front-end,
    // which turned really ugly (but now we know until next time)

    // The central idea was to use only one end-point that can be called with different
    // methods to manipulate and/or fetch contacts

    // In the cases of operations that requires only one contact to target (modifying/deleting)
    // we decided that the user had to provide a specific enough search term for names until only
    // one search result was found.


    // The contact GET mapping takes in a name parameter to filter the contacts.
    // This returns a thymeleaf page listing all contact entries where the name contains
    // the search term.
    @GetMapping("/contact")
    public String getContacts(@RequestParam(value="name", required=false, defaultValue = "") String name, Model model) {
        ResultSet resultSet;
        System.out.println(name);
        if(name.isEmpty())
        {
            resultSet = Database.database.getResultSetFromQuery("SELECT * FROM contactList");
        } else {
            String preparedStatement = String.format("SELECT * FROM contactList WHERE name like '%%%s%%'",name);
            resultSet = Database.database.getResultSetFromQuery(preparedStatement);
        }
        ArrayList<Contact> contactArrayList = getContactListFromResultSet(resultSet);
        model.addAttribute("contactList", contactArrayList);
        return "presentContacts";
    }

    // A help function reading contacts into an array from a result set
    // (as it's being used a lot throughout this file)
    private ArrayList<Contact> getContactListFromResultSet(ResultSet resultSet) {
        ArrayList<Contact> contactArrayList = new ArrayList<>();
        try {
            while (resultSet.next()) {
                contactArrayList.add(new Contact(
                        resultSet.getString("name"),
                        resultSet.getString("email"),
                        resultSet.getString("contactNumber")
                ));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return null;
        }
        return contactArrayList;
    }

    // The contact DELETE mapping uses a model attribute as a parameter (contact).
    // Since only the name is necessary
    @PostMapping("/delete")
    public String deleteContact(@ModelAttribute (value = "contact") Contact contact, Model model) {
        System.out.println("Contact: " + contact.name);

        ResultSet resultSet;
        if(contact.name.isEmpty())
        {
            return "failed";
        } else {
            String preparedStatement = String.format("SELECT * FROM contactList WHERE name like '%%%s%%'",contact.name);
            resultSet = Database.database.getResultSetFromQuery(preparedStatement);
        }
        ArrayList<Contact> contactlist = getContactListFromResultSet(resultSet);
        if (contactlist.size() ==1) {

            String preparedStatement = String.format("DELETE FROM contactList WHERE name like '%%%s%%'",contact.name);
            Database.database.executeStatement(preparedStatement);

            model.addAttribute("contact", contactlist.get(0));

            return "deletedContact";

        } else {
            return "failed";
        }

    }

    // The PATCH mapping uses a request parameter (name) to find a contact to update with information in
    // the JSON body sent. The content of the body doesn't have to be complete, which means only
    // the variables present in the body will be updated in the database.
    @PatchMapping("/contact")
    public String updateContact(@RequestBody Contact updateContact, @RequestParam(value="name", required=false, defaultValue = "") String name, Model model) {

        ResultSet resultSet;
        if(name.isEmpty())
        {
            return "failed";
        } else {
            // Look for contacts that are like the provided name
            String preparedStatement = String.format("SELECT * FROM contactList WHERE name like '%%%s%%'",name);
            resultSet = Database.database.getResultSetFromQuery(preparedStatement);
        }
        // Fill a list with matches
        ArrayList<Contact> contactlist = getContactListFromResultSet(resultSet);
        // If only one match, we'll modify it
        if (contactlist.size() ==1) {

            String fullName = contactlist.get(0).name;

            // This string builder and if-statements constructs the prepared statement
            // with the information to be updated
            StringBuilder stringBuilder = new StringBuilder();
            if(updateContact.name != null) {
                stringBuilder.append(String.format("name='%s'",updateContact.name));
            } else {
                updateContact.name = contactlist.get(0).name;
            }
            if(updateContact.email != null) {
                if(stringBuilder.length() != 0) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(String.format("email='%s'", updateContact.email));
            } else {
                updateContact.email = contactlist.get(0).email;
            }
            if(updateContact.contactNumber != null) {
                if(stringBuilder.length() != 0) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(String.format("contactNumber='%s'", updateContact.contactNumber));
            } else {
                updateContact.contactNumber = contactlist.get(0).contactNumber;
            }

            String preparedStatement = String.format("UPDATE contactList SET %s WHERE name='%s'",stringBuilder.toString(), fullName);

            System.out.println("Patch query: "+preparedStatement);

            // Execute the prepared statement
            Database.database.executeStatement(preparedStatement);

            // Display in thymeleaf what has been updated
            model.addAttribute("oldContact", contactlist.get(0));
            model.addAttribute("newContact", updateContact);

            return "updateContact";

        } else {
            return "failed";
        }

    }

    // The POST mapping gets information through the request body and inserts it into the database.
    // No checks for duplicates is used here
    @PostMapping("/contact")
    public String insertContact(@RequestBody Contact contact, Model model) {
        System.out.println("here");
            String preparedStatement = String.format("INSERT INTO contactList(name, email, contactNumber) VALUES (\"%s\", \"%s\" ,\"%s\")", contact.getName(), contact.getEmail(), contact.getContactNumber());
            Database.database.executeStatement(preparedStatement);
            model.addAttribute("contact", contact);
            return "addContact";
    }

    // This is kind of a redirect mapping as a solution to how we built things.
    // It takes the parameters sent by POST and returns a thymepage with the contact details
    // filled into the input fields
    @PostMapping("/editContact")
    public String editContact(@ModelAttribute (value = "contact") Contact contact, Model model) {
        model.addAttribute("name", contact.name);
        model.addAttribute("email", contact.email);
        model.addAttribute("contactNumber", contact.contactNumber);
        return "editContact";
    }



}
