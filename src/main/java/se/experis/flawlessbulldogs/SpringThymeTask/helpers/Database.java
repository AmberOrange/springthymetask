package se.experis.flawlessbulldogs.SpringThymeTask.helpers;

import java.sql.*;

public class Database {
    private Connection conn;
    public static Database database;

    public Database(String databaseLocation) {
        openConnection(databaseLocation);
    }

    public Database() { }

    // Opens a connection to the desired database
    public boolean openConnection(String databaseLocation) {
        StringBuilder stringBuilder = new StringBuilder(databaseLocation);
        stringBuilder.insert(0, "jdbc:sqlite::");

        try {
            conn = DriverManager.getConnection(stringBuilder.toString());

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
        return true;
    }

    // Check if the database connection is open at the moment
    public boolean isConnected() {
        // conn.isClosed() throws an exception that can't be ignored.
        // Encapsulate the method call with try-catch
        try {
            return conn != null && !conn.isClosed();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
    }

    // Close the connection to the database
    public boolean closeConnection() {
        if(!isConnected()) {
            return false;
        }
        try {
            conn.close();
            return true;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
    }


    // Executes a query on the opened database (if open)
    // and returns a ResultSet
    public ResultSet getResultSetFromQuery(String statement) {
        if(!isConnected()) {
            return null;
        } else {
            try {
                PreparedStatement preparedStatement = conn.prepareStatement(statement);
                return preparedStatement.executeQuery();
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
                return null;
            }
        }
    }
    public void executeStatement(String statement) {
        if(!isConnected()) {
            return;
        } else {
            try {
                PreparedStatement preparedStatement = conn.prepareStatement(statement);
                preparedStatement.execute();
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
                return;
            }
        }
    }
}